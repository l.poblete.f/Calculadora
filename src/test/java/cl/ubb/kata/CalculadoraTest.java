package cl.ubb.kata;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculadoraTest {
	
	private int resultado;

	@Test
	public void SumarUnoMasUnoIgual2() {
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.sumar(1,1);
		
		assertEquals(2,resultado);
	}

	@Test
	public void SumarUnoPositivoMasDosNegativoIgualaMenosUno() {
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.sumar(1,-2);
		
		assertEquals(-1,resultado);
	}
	
	@Test
	public void SumarUnoNegativoMasUnoNegativoIgualaMenosDos() {
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.sumar(-1,-1);
		
		assertEquals(-2,resultado);
	}
	
	@Test
	public void MultiplicarDosPorTresIgualASeis() {
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.multiplicar(2,3);
		
		assertEquals(6,resultado);
	}
	
	@Test
	public void MultiplicarMenosDosPorMenosTresIgualASeis() {
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.multiplicar(-2,-3);
		
		assertEquals(6,resultado);
	}
	
	
	@Test
	public void DividirCuatroPorDosIgualaDos() throws DivisionException{
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.dividir(4,2);
		
		assertEquals(2,resultado);
	}
	
	@Test
	public void DividirSeisPorDosIgualaTres() throws DivisionException{
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.dividir(6,2);
		
		assertEquals(3,resultado);
	}
	
	@Test (expected=DivisionException.class)
	public void DividirCuatroPorCeroRetornaExcepcionPorDivisionPorCero() throws DivisionException{
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.dividir(4,0);
		
		
	}
	
	
	@Test
	public void DividirOchoPorMenosDosIgualaMenosCuatro() throws DivisionException{
		
				
		Calculadora cal=new Calculadora();
		
		resultado=cal.dividir(8,-2);
		
		assertEquals(-4,resultado);
	}
	
	
	
	
	

	
}
